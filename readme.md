# Vim Rust IDE

To create image and to test, run:

```
podman build -t g00n1x/vim-rust .
podman run -td --name rust-ide g00n1x/vim-rust
podman exec -it rust-ide fish
```

You can also mount git projects or your SSH key in case you need git operations.
