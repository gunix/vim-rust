source $VIMRUNTIME/defaults.vim
set mouse=""
set nu
set laststatus=2
set t_Co=256
set wildmode=longest,list
nnoremap <C-Up> <C-W><C-K>
nnoremap <C-Down> <C-W><C-J>
nnoremap <C-Right> <C-W><C-L>
nnoremap <C-Left> <C-W><C-H>
nnoremap <F2> :set invnu <CR>
nnoremap <F3> :set mouse=a <CR>
nnoremap <F4> :set mouse="" <CR>
nnoremap <F5> :terminal % <CR>

colorscheme pablo
hi Folded ctermbg=232 ctermfg=33
set splitbelow
set splitright
set shiftwidth=2
set expandtab
set softtabstop=2
set foldmethod=syntax
set foldnestmax=1

autocmd BufWritePost * GitGutter
autocmd BufNewFile,BufRead *.fish set syntax=bash

highlight SignColumn ctermbg=none
highlight GitGutterAdd    ctermfg=12
highlight GitGutterChange ctermfg=13
highlight GitGutterDelete ctermfg=1
let g:gitgutter_set_sign_backgrounds = 1

let g:racer_experimental_completer = 1
let g:racer_insert_paren = 1

let g:airline#extensions#tabline#enabled = 1
let g:airline_powerline_fonts = 1
